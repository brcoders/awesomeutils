package es.esy.brcoders.awesome;

import es.esy.brcoders.awesome.commands.healeR;
import es.esy.brcoders.awesome.commands.setSpawn;
import es.esy.brcoders.awesome.commands.spawN;
import es.esy.brcoders.awesome.commands.tpA;
import es.esy.brcoders.awesome.events.healSign;
import es.esy.brcoders.awesome.events.spawnSign;
import java.util.logging.Level;
import org.bukkit.plugin.PluginDescriptionFile;
import org.bukkit.plugin.java.JavaPlugin;

public class Awesome_Plugin extends JavaPlugin
{
    //Variaveis de Classe
    PluginDescriptionFile pdFile = this.getDescription();
    
    //Codigo Principal
    @Override
    public void onEnable()                  //Quando o servidor iniciar rodar estes comandos
    {
        //Registrar Eventos aqui
        getServer().getPluginManager().registerEvents(new healSign(), this);
        getServer().getPluginManager().registerEvents(new spawnSign(), this);
        
        //Registrar Comandos aqui
        //Comandos de OP
        this.getCommand("heal").setExecutor(new healeR());
        
        //Comandos normais
        this.getCommand("tpa").setExecutor(new tpA());
        this.getCommand("tpsim").setExecutor(new tpA());
        this.getCommand("tpnao").setExecutor(new tpA());
        this.getCommand("spawn").setExecutor(new spawN());
        this.getCommand("setspawn").setExecutor(new setSpawn());
        
        //Outros
        getLogger().log(Level.INFO, "{0} {1} esta ligado!", new Object[]{pdFile.getName(), pdFile.getVersion()});
    }
    
    @Override
    public void onDisable()
    {
        //Mensagem no Log do server. Não precisa desregistrar eventos.
        getLogger().info("Plugin Desativado!");
    }
}