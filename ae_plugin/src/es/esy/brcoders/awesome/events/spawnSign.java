package es.esy.brcoders.awesome.events;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.Sign;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

/**
 * @since 28/09/2015
 * @author mint
 * 
 * Placa de Spawn
 * 
 */
public class spawnSign implements Listener
{
    public void onPlayerClickInSign(PlayerInteractEvent e)
    {
        if(e.getAction().equals(Action.RIGHT_CLICK_BLOCK) || e.getAction().equals(Action.LEFT_CLICK_BLOCK))
        {
            if(e.getClickedBlock().getType() == Material.SIGN || e.getClickedBlock().getType() == Material.SIGN_POST)
            {
                Block block = e.getClickedBlock();
                Sign sign = (Sign) block;
                
                String firstLine = sign.getLine(0);
                String secondLine = sign.getLine(0);
                
                if(firstLine.equalsIgnoreCase("[AWESOME]"))
                {
                    if(secondLine.equalsIgnoreCase("[SPAWN]"))
                    {
                        Location spawn = e.getPlayer().getWorld().getSpawnLocation();
                        e.getPlayer().teleport(spawn);
                        e.getPlayer().sendMessage("Teleportado com sucesso!");
                    }
                }                
            }
        }
    }
}