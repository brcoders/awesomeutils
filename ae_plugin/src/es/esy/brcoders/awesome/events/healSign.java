package es.esy.brcoders.awesome.events;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.Sign;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

/**
 * @since 28/09/2015
 * @author mint
 * 
 * Placa de Healing
 * 
 */

public class healSign implements Listener
{    
    @EventHandler    
    public void onPlayerClickInSign(PlayerInteractEvent e)
    {
        if(e.getAction().equals(Action.RIGHT_CLICK_AIR))
        {
            if(e.getClickedBlock().getType() == Material.SIGN || e.getClickedBlock().getType() == Material.SIGN_POST)
            {
                Block block = e.getClickedBlock();
                Sign sign = (Sign) block.getState();
                
                String firstLine = sign.getLine(0);
                String secondLine = sign.getLine(0);
                String thirdLine = sign.getLine(0);
                
                if(e.getPlayer().hasPermission("awesome.sign.use") && e.getPlayer().isOp())
                {
                    if(firstLine.equalsIgnoreCase("[AWESOME]"))
                    {
                        e.getPlayer().sendMessage("[AWESOME] Usos possiveis: [HEAL], [SPAWN], [KILL], [CLEAR]");
                        
                        if(e.getPlayer().hasPermission("awesome.sign.use.heal") && e.getPlayer().isOp())
                        {
                            if(secondLine.equalsIgnoreCase("[HEAL]"))
                            {
                                e.getPlayer().setHealth(20);
                                
                                if(e.getPlayer().hasPermission("awesome.sign.use.heal.op") && e.getPlayer().isOp())
                                {                                                           
                                    if(thirdLine.equalsIgnoreCase("[OP-ONLY]"))
                                    {
                                        e.getPlayer().setHealth(20);
                                        e.getPlayer().setFoodLevel(20);
                                        e.getPlayer().setFireTicks(0);                                        
                                    }
                                } else {
                                    e.getPlayer().sendMessage("Essa placa é so para pessoas permitidas!");
                                    }                        
                            }
                        } else {
                            e.getPlayer().sendMessage("Essa placa é so para pessoas permitidas!");
                            }
                    }
                } else {
                    e.getPlayer().sendMessage("Você nao tem permissão para utilizar placas!");
                }
            }
        }
    }
}