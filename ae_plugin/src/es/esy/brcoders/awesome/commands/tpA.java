package es.esy.brcoders.awesome.commands;

import java.util.HashMap;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * @since 28/09/2015
 * @author mint
 * 
 * Sistema de TPA
 * 
 */
public class tpA implements CommandExecutor{
    //Variaveis de Classe
    HashMap<Player, Player> tpa = new HashMap<>();
     
    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args)
    {
        Player s = (Player) sender;                     //Variavel 's' do tipo Player recebe sender usando um cast.
        if(command.getName().equalsIgnoreCase("tpa"))
        {                                               //Checa se oque foi digitado é igual ao definido se retornar true executa.
            if(!(sender instanceof Player))
            {                                           //Checa se quem esta enviando o comando é um Player ou o Console.
                sender.sendMessage("Você precisa ser um player!");
                return false;
            } else {
                if(args.length < 1 || args.length > 1)
                {
                    s.sendMessage("Argumentos invalidos ou Insuficientes. Usage: /tpa || /tpa <player>");
                    return false;
                }
                if(args.length == 1)                    //Checa argumentos se args.length == 1
                {
                    Player t = s.getServer().getPlayer(args[0]);
                    if(t != null)                       //Checa se o jogador alvo esta online se sim adiciona os nicks na Array
                    {
                        tpa.put(t, s);
                        t.sendMessage(s.getName() + " esta pedindo tpa. Use /tpsim ou /tpnao");
                        return true;
                    } else {
                        s.sendMessage(args[0] + " esta offline!");
                        return false;
                    }
                }
            }
        } else if(command.getName().equalsIgnoreCase("tpsim"))      //Comando imbutido
        {
            if(tpa.get(s) != null)                                  //Se 's' for diferente de 'null' executar se nao joga uma Exception
            {
                tpa.get(s).teleport(s);
                s.sendMessage("Você aceitou a solicitação!");
                tpa.get(s).sendMessage("Você foi teleportado!");
                tpa.put(s, null);
            } else {
                throw new ArrayStoreException("Não pode ser Null!");
            }
        } else if(command.getName().equalsIgnoreCase("tpnao"))
        {
            if(tpa.get(s) != null)                                  //Se 's' for diferente de 'null' executar se nao joga uma Exception
            {
                s.sendMessage("Você negou a solicitação!");
                tpa.get(s).sendMessage("Você não foi teleportado!");
                tpa.put(s, null);
            } else {
                throw new ArrayStoreException("Não pode ser Null!");
            }
        }
        return false;
    }
}