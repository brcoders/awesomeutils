package es.esy.brcoders.awesome.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * @since 28/09/2015
 * @author mint
 * 
 * Sistema de SETSPAWN
 * 
 */
public class spawN implements CommandExecutor
{

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args)
    {
        //Checa se o 'sender' é um player ou o console
        if(sender instanceof Player)
        {
            //Se for um player cria variavel 'p' do tipo Player e agrega o sender na variavel 'p' usando um cast (Player)
            Player p = (Player) sender;
            
            //Se o player digitar /spawn executa esse código
            if(command.getName().equalsIgnoreCase("spawn"))
            {
                //Se ele tiver permissão ou for OP teleporta o player para o spawn do mundo.
                if(p.hasPermission("awesome.spawn.use") || p.isOp() == true)
                {
                    p.teleport(p.getWorld().getSpawnLocation());
                    p.sendMessage("Teleportado com sucesso!");
                }else{
                    //Senao diz que nao tem permissao e retorna 'true' (porque o onCommand é um boolean)
                    p.sendMessage("Você não tem permissão!");
                    return true;
                }
            }
        } else {
            //Se não for player...
            sender.sendMessage("Você não é um player!");
            return true;            
        }
        
        return true;
    }
    
}