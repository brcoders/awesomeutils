package es.esy.brcoders.awesome.commands;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * @since 28/09/2015
 * @author mint
 * 
 * Sistema de Healing
 * 
 */
public class healeR implements CommandExecutor
{    
    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
       if(command.getName().equalsIgnoreCase("heal"))
       {
           if(sender.isOp() == true)                                    //checa se o sender é OP, se sim executa o código senao retorna mensagem
                if(!(sender instanceof Player))                         //Checa se o sender é player ou console
                {
                    sender.sendMessage("Você precisa ser um player!");
                    return false;
                } else {
                    Player s = (Player) sender;
                    if(args.length == 0)                                //Se length = 0 curar você mesmo
                    {
                        s.setHealth(20);
                        s.setFireTicks(0);
                        s.setFoodLevel(20);
                        s.sendMessage(ChatColor.GREEN + "Você foi curado!");
                        return true;
                    } else if(args.length == 1)                         //Se length = 1 curar outro player
                    {
                        String playerName = args[0];
                        Player otherPlayer = s.getServer().getPlayer(playerName);
                        if(otherPlayer == null)                        //Checa se o alvo esta offline ou online se nao cura
                        {
                            s.sendMessage(ChatColor.RED + args[0] + " jogador esta offline!");
                            return false;
                        } else {
                            otherPlayer.setHealth(20);
                            otherPlayer.setFireTicks(0);
                            otherPlayer.setFoodLevel(20);
                            otherPlayer.sendMessage("Foste curado por: " + ChatColor.GREEN + s.getName());
                            return true;
                        }             
                    } else if(args.length > 1)                         //Se arguementos maiores que 1 retorna erro
                    {
                        s.sendMessage("Muitos argumentos! Usage: /heal or /heal <player>");
                        return false;
                    }
            } else {
               sender.sendMessage("Você não é OP!");
            }
        }
        return false;
    }
}