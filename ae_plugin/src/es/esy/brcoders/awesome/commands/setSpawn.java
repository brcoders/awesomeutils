package es.esy.brcoders.awesome.commands;

import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 *
 * @author mint
 */
public class setSpawn implements CommandExecutor
{

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args)
    {
        //Checa se o 'sender' é um player ou o console
        if(sender instanceof Player)
        {
            //Se for um player cria variavel 'p' do tipo Player e agrega o sender na variavel 'p' usando um cast (Player)
            Player p = (Player) sender;
            
            //Se o player digitar /spawn executa esse código
            if(command.getName().equalsIgnoreCase("setspawn"))
            {
                World w = p.getWorld();                         //Variavel 'w' do tipo World
                
                int x = p.getLocation().getBlockX();            //Inteiro X recebe a posição X do player
                int y = p.getLocation().getBlockY();            //Inteiro Y recebe a posição Y do player
                int z = p.getLocation().getBlockZ();            //Inteiro Z recebe a posição Z do player
                
                p.getWorld().setSpawnLocation(x, y, z);         //Seta o spawn do mundo
                p.sendMessage("Novo spawn setado em: "+x+" "+y+" "+z);
                return true;
            }
        } else {
            //Senao...
            sender.sendMessage("Você não é um player!");
            return true; 
        }
        return false;
    }
    
}
